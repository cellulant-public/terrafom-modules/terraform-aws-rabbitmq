#!/usr/bin/env bash
DNSES=$(aws ec2 describe-instances --region eu-west-1 --profile cellulant-payments --filters "Name=tag:aws:autoscaling:groupName,Values=rabbitmq-tingg6-mq" "Name=instance-state-name,Values=running" | jq ".Reservations[].Instances[].PrivateDnsName" | xargs)

HOSTNAMES=()
for dns in $DNSES; do
    if [ "$dns" != "$HOSTNAME" ]; then
        HOSTNAMES+=( $dns )
    fi
done

echo $HOSTNAMES